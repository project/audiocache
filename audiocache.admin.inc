<?php // $Id$ 

/**
 * Preset Admin callbacks and required functions.
 */

function audiocache_preset_overview_page(){
  $header = array(t('Preset Name'), t('Actions'));
  $rows = array();
  // Always clear the preset cache on this display.
  foreach (audiocache_presets() as $preset) {
    $row = array();
    $row[] = l($preset['name'], 'admin/build/audiocache/'. $preset['pid']);
    $links = array();

    $links[] = l(t('Edit'), 'admin/build/audiocache/'. $preset['pid']);
    $links[] = l(t('Delete'), 'admin/build/audiocache/'. $preset['pid'] .'/delete');

    $row[] = implode('&nbsp;&nbsp;&nbsp;&nbsp;', $links);
    $rows[] = $row;
  }
  $output = theme('table', $header, $rows); 

  return $output;
}


function audiocache_preset_delete_form($form_state, $preset = array()) {
  if (empty($preset)) {
    drupal_set_message(t('The specified preset was not found'), 'error');
    drupal_goto('admin/build/audiocache');
  }

  $form = array();
  $form['pid'] = array('#type' => 'value', '#value' => $preset['pid']);
  return confirm_form(
    $form,
    t('Are you sure you want to delete the preset %preset?',
      array('%preset' => $preset['name'])
    ),
    'admin/build/audiocache',
    t('This action cannot be undone.'),
    t('Delete'),  t('Cancel')
  );
}


function audiocache_preset_delete_form_submit($form, &$form_state) {
  $preset = audiocache_preset($form_state['values']['pid']);
  audiocache_preset_delete($preset);
  drupal_set_message(t('Preset %name (ID: @id) was deleted.', array('%name' => $preset['name'], '@id' => $preset['pid'])));
  $form_state['redirect'] = 'admin/build/audiocache';
}


/**
 * Preset definition form
 */
function audiocache_preset_form($form_state, $preset = array()) {
  $form = array();
  	
  $form['pid'] = array(
    '#type' => 'value',
    '#value' => $preset['pid'],
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#size' => '64',
    '#title' => t('Preset Name'),
    '#default_value' => $preset['name'],
    '#description' => t('Please only use alphanumeric characters, underscores (_), and hyphens (-) for preset names.'),
    '#validate' => array('audiocache_element_name_validate' => array()),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Preset'),
    '#weight' => 10,
  );
  
  
  // get available formats from SoX API
  $formats = sox_formats();
  $formatselect = array();
  foreach($formats as $f => $o){
  	$formatselect[$f] = $o['#title']; 
  }
  
  
  $form['format'] = array(
    '#type' => 'select',
    '#title' => t('Output format'),
    '#description' => t('Define the output format of this preset. If "-" is chosen, the file gets only copied.'),
    '#options' => $formatselect,
    '#default_value' => !empty($preset['format']) ? $preset['format'] : 0,
    '#ahah' => array(
      'path' => 'admin/build/audiocache/'.$preset['pid'].'/preset_form_ahah',
      'method' => 'replace',
      'wrapper' => 'format-options-wrapper'
    )
  );
  
  
  /**
   * oh god, this is crap below. i am to tired to fix this now
   */
  if(isset($form_state['values']['format'])){
  	$selected_format = $form_state['values']['format'];
  	$formatoptions = $formats[$selected_format];
  	
  } else if(!empty($preset['format']) && !empty($preset['options'])){
  	$selected_format = $preset['format'];
  	$formatoptions = $formats[$selected_format];
  	
    foreach($preset['options'] as $key => $value)
      $formatoptions[$key]['#default_value'] = $value;
      
  } else $formatoptions = $formats[0];
  

  
  $form['format-wrapper']= array(
    '#tree' => FALSE,
    '#prefix' => '<div id="format-options-wrapper">',
    '#suffix' => '</div>',
  );
  
  $form['format-wrapper']['options'] = _audiocache_preset_format_form($formatoptions);
  
  $form['testpreset'] = _audiocache_preset_format_test($preset);
  
  return $form;
}

/**
 * Build a format options form
 */
function _audiocache_preset_format_form($options){
  $form = array(
    '#type' => 'fieldset',
    '#title' => 'Format options',
    '#tree' => TRUE
  );
  
  $form += $options;
  
  return $form;
}


/**
 * Do a test with the last saved options
 */
function _audiocache_preset_format_test($preset){
  if(empty($preset['format']) && empty($preset['options'])) return array();
  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Test output')
  );
  
  $testoutput = _audiocache_build_test_derivative($preset);
  
  $form['output'] = array(
    '#type' => 'markup',
    '#value' => 'Download: ' . $testoutput
  );
  
  return $form;
}


function audiocache_preset_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Check for duplicates
  foreach (audiocache_presets() as $preset) {
    if ($values['name'] == $preset['name'] && $values['pid'] != $preset['pid']) {
      form_set_error('name', t('The preset name you have chosen is already in use.'));
      break;
    }
  }
  // Check for illegal characters in preset names
  if (empty($values['name']) || preg_match('/[^0-9a-zA-Z_\-]/', $values['name'])) {
    form_set_error('name', t('Please only use alphanumeric characters, underscores (_), and hyphens (-) for preset names.'));
  }
  
  // TODO
  // check against formats again
}


function audiocache_preset_form_submit($form, &$form_state) {
  // submit was not called by submit button
  if(!isset($form_state['values']['op'])) return;
  
  $preset = audiocache_preset_save($form_state['values']);
  
  // Push back to the preset form
  $form_state['redirect'] = 'admin/build/audiocache/'. $preset['pid'];
}


/**
 * AHAH callback for Format forms
 */
function audiocache_preset_form_ahah(){
  module_load_include('inc', 'audiocache', 'audiocache.admin');
  
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  
  $form_state['submitted'] = TRUE;

  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
    
  $element = $form['format-wrapper'];
  unset($element['#prefix'], $element['#suffix']);

  drupal_json( array(
    'status' => TRUE, 
    'data' => theme('status_messages') . drupal_render($element),
    'settings'  => array('ahah' => $settings['ahah'])
  ));
}

