<?php // $Id$ 

/**
 * AudioCache module
 * Processes and converts audio files to different formats
 * @author Alex Bool (lostace.com)
 */

define('AUDIOCACHE_SETTINGS','audiocache_settings');

/**
 * Implementation of hook_perm().
 */
function audiocache_perm() {
  return array(
    'administer audiocache'
  );
}

/*
 * Implementation of hook_menu
 */
function audiocache_menu(){
  $items = array();
  
  $items['admin/build/audiocache'] = array(
    'title' => 'AudioCache',
    'file' => 'audiocache.admin.inc',
    'description' => 'Configure AudioCache presets',
    'page callback' => 'audiocache_preset_overview_page',
    'access arguments' => array('administer audiocache'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/build/audiocache/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  ); 
  
  $items['admin/build/audiocache/add'] = array(
    'title' => 'Add new preset',
    'file' => 'audiocache.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('audiocache_preset_form'),
    'access arguments' => array('administer audiocache'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/build/audiocache/%audiocache_preset'] = array(
    'title callback' => 'audiocache_preset_title_callback',
    'title arguments' => array('Edit preset: !name', 3),
    'file' => 'audiocache.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('audiocache_preset_form', 3),
    'access arguments' => array('administer audiocache'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/build/audiocache/%audiocache_preset/delete'] = array(
    'title callback' => 'audiocache_preset_title_callback',
    'title arguments' => array('Delete preset: !name', 3),
    'file' => 'audiocache.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('audiocache_preset_delete_form', 3),
    'access arguments' => array('administer audiocache'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/build/audiocache/%audiocache_preset/preset_form_ahah'] = array(
    'page callback' => 'audiocache_preset_form_ahah',
    'access arguments' => array('administer audiocache'),
    'file' => 'audiocache.admin.inc',
    'type' => MENU_CALLBACK
  );
  
  return $items;
}

/**
 * Menu wildcard loader.
 */
function audiocache_preset_load($pid) {
  return audiocache_preset($pid);
}

function audiocache_preset_title_callback($title, $preset = array(), $action = array()) {
  $replacements = array();
  if (!empty($preset)) {
    $replacements['!name'] = $preset['name'];
    $replacements['!pid'] = $preset['pid'];
  }
  return t($title, $replacements);
}


/**
 * Get default values for AudioCache settings
 */
function audiocache_settings($new = NULL){
  static $settings;
  if($new) $settings = variable_set(MS_RELEASE_SETTINGS, $new);
  else if(!$settings){
    $default = array(
      'api_key' => ''
    );
    $settings = variable_get(AUDIOCACHE_SETTINGS, $default);
  }
  return $settings;
}




/**
 * Get all AudioCache presets
 */
function audiocache_presets() {
  static $presets = array();

  // Return presets if the array is populated.
  if (!empty($presets)) {
    return $presets;
  }

  $result = db_query('SELECT * FROM {audiocache_preset} ORDER BY name');
  while ($preset = db_fetch_array($result)) {
  	$preset['options'] = unserialize($preset['options']);
  	$preset['effects'] = unserialize($preset['effects']);
  	$preset['gopts'] = unserialize($preset['gopts']);
    $presets[$preset['pid']] = $preset;
  }
    
  return $presets;
}

/**
 * Load a preset by pid.
 */
function audiocache_preset($pid) {
  $presets = audiocache_presets();
  return (isset($presets[$pid])) ? $presets[$pid] : array();
}

/**
 * Load a preset by name.
 */
function audiocache_preset_by_name($name) {
  static $presets_by_name = array();
  if (!$presets_by_name &&  $presets = audiocache_presets()) {
    foreach ($presets as $preset) {
      $presets_by_name[$preset['name']] = $preset;
    }
  }
  return (isset($presets_by_name[$name])) ? $presets_by_name[$name] : array();
}


/**
 * Save an AudioCache preset.
 */
function audiocache_preset_save($preset) {
  // @todo: CRUD level validation?
  if (isset($preset['pid']) && is_numeric($preset['pid'])) {
    drupal_write_record('audiocache_preset', $preset, 'pid');
  }
  else {
    drupal_write_record('audiocache_preset', $preset);
  }

  return $preset;
}

/**
 * Delete an AudioCache preset
 */
function audiocache_preset_delete($preset) {
  db_query('DELETE FROM {audiocache_preset} where pid = %d', $preset['pid']);
  return TRUE;
}



/**
 * Create a new audio file based on a preset.
 * 
 * @return 
 *   the path of the new file or FALSE if request could not be completed
 */
function audiocache_build_derivative($preset, $src, $dst) {
  // get the folder for the final location of this preset...
  $dir = dirname($dst);

  // Build the destination folder tree if it doesn't already exists.
  if (!file_check_directory($dir, FILE_CREATE_DIRECTORY) && !mkdir($dir, 0775, TRUE)) {
    watchdog('AudioCache', 'Failed to create AudioCache directory: %dir', array('%dir' => $dir), WATCHDOG_ERROR);
    return FALSE;
  }

  // recreate destination file name based on preset format
  $dst = _audiocache_clean_file_extension($dst, $preset);

  // Simply copy the file if there is no preset format set.
  if (!$preset['format']) {
  	$dst .= _audiocache_get_file_extension($src);
    $result = file_copy($src, $dst, FILE_EXISTS_REPLACE);
  } else {
    // try to build it
    $result = sox_exec($src, $dst, $preset['format'], $preset['options']);
  }

  if(!$result){ 
  	watchdog('AudioCache', t('Could not complete AudioCache request for: %path'), array('%path' => $src), WATCHDOG_ERROR);
  	return FALSE;
  } else return $dst;
}



/**
 * Build a test file for a given preset
 */
function _audiocache_build_test_derivative($preset){
  // generate paths for test file
  $inpath = drupal_get_path('module', 'audiocache') . '/test.wav';
  $outpath = file_directory_path().'/test';
  
  // try building derivative
  $result = audiocache_build_derivative($preset, $inpath, $outpath);
  
  // return link to new file
  return $result ? l(_audiocache_strip_file_directory($result), $result) : '';
}


/**
 * Remove a possible leading file directory path from the given path.
 */
function _audiocache_strip_file_directory($path) {
  $dirpath = dirname($path);
  $dirlen = strlen($dirpath);
  if (substr($path, 0, $dirlen + 1) == $dirpath .'/') {
    $path = substr($path, $dirlen + 1);
  }
  return $path;
}

/**
 * Get file extension for a given file
 */
function _audiocache_get_file_extension($filepath){
  return substr(strrchr($filepath, '.'), 1 );
}

/**
 * Remove file extension for a given file
 */
function _audiocache_strip_file_extension($filepath){
  return substr($filepath, 0, stripos($filepath, '.'));
}

/**
 * Give path a clean file extension based on preset format
 */
function _audiocache_clean_file_extension($filepath, $preset){
  $ext = _audiocache_get_file_extension(_audiocache_strip_file_directory($filepath));
  if($ext) $filepath = _audiocache_strip_file_extension($filepath);
  return $filepath .'.'. ($preset['format'] ? $preset['format'] : $ext);
}

